import Debug from 'debug'
import { describe } from 'riteway'
import { SharepointJSOMResource } from '../index.js'
import fs from 'fs'
const debug = Debug('test')


describe('Create SP File', async (assert) => {
  try {

    const config = {
      environment: {
        SHAREPOINT_USERNAME: "alliance@quartileone.com",
        SHAREPOINT_PASSWORD: "F2cmSpYF63wh"
      }
    }

    const options = {
      sharepointUrl: 'https://technologyincubator.sharepoint.com/sites/GDDev3',
      sharepointTenant: 'https://technologyincubator.sharepoint.com',
      sharepointSite: '/sites/GDDev3'
    }

    let client = await SharepointJSOMResource(config,options)

    // const fileContent = fs.readFileSync('test.docx')
    // const result = client.createFile("test.docx",fileContent)

    const fileContent = fs.readFileSync('test.xlsx')
    const result = await client.upsertFile("unittest.xlsx",fileContent)

    console.log(result)

    assert({
      given: 'Some file content',
      should: "Create in SP",
      actual: result.editUrl!==undefined && result.downloadUrl!==undefined,
      expected: true
    })
  }catch(ex){
    console.log(ex)
  }
})
