import Debug from 'debug'
import * as spauth from 'node-sp-auth';
import * as request from 'request-promise';
const debug = Debug('cli')

export const LANGUAGE_ENGLISH=1033

//Sharepoint Client API : https://docs.microsoft.com/en-us/previous-versions/office/sharepoint-csom/ee544361(v=office.15)
//https://docs.microsoft.com/en-us/sharepoint/dev/sp-add-ins/complete-basic-operations-using-javascript-library-code-in-sharepoint#sharepoint-list-tasks

//let stripWhiteSpace = (x)=>x.replace(' ','_')
let stripWhiteSpace = (x)=>x.replace(/ /g,'')

export const SharepointJSOMResource = async (config,options)=>{

  const url = options.sharepointTenant + options.sharepointSite  

  const credentialOptions = {
    username: config.environment.SHAREPOINT_USERNAME,
    password: config.environment.SHAREPOINT_PASSWORD
  }

  debug(`Connectivity to sharepoint as ${credentialOptions.username}`)

  const JsomNode = require('sp-jsom-node').JsomNode;

  const jsomNode  = new JsomNode({modules: [ 'taxonomy','project']})
  const ijsomNode = jsomNode.init({siteUrl: url, authOptions: credentialOptions})
  const ctx = ijsomNode.getContext()

  debug(`Created Context for Site ${url}`)
  
  let taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(ctx)
    
  let termStore = taxonomySession.getDefaultSiteCollectionTermStore()

  const createTermGroup = async (groupName)=>{
  
    let existing = termStore.get_groups()
    ctx.load(existing);
    await ctx.executeQueryPromise()
    let dt  = existing.get_data()
    if(dt.find(g=>g.get_name()===groupName)){
      debug(`Term Group Exists ${groupName}`)
      return
    }
    let guid = SP.Guid.newGuid()
    termStore.createGroup(groupName,guid)
    ctx.load(termStore);
    await ctx.executeQueryPromise()
    let termGroup = termStore.get_groups().getByName(groupName)
    return termGroup
  }

  const createTermSet = async (groupName,termsetName)=>{
    let termGroup = await getTermGroup(groupName)
    let termsets =  termGroup.get_termSets()
    ctx.load(termsets);
    await ctx.executeQueryPromise()

    let existing = termsets.get_data().find(t=>t.get_name()===termsetName)
    if(existing){
      debug(`TermSet exists ${termsetName}`)
      return existing
    }

    debug(`creating TermSet ${termsetName}`)
    let guid = SP.Guid.newGuid()
    let termset = termGroup.createTermSet(termsetName,guid,1033)
    
    ctx.load(termset);

    try {
      await ctx.executeQueryPromise()
      return termset
    }catch(ex){
      throw ex
    }
  }

  const createTerm = async (termParent,termName)=>{
    let terms = termParent.get_terms()
    ctx.load(terms);
    await ctx.executeQueryPromise()
    let existing = terms.get_data().find(t=>t.get_name()===termName)
    if(existing){
      debug(`Term Exists: ${termName}`)
      return existing
    }
    debug(`Creating Term: ${termName}`)
      
    let guid = SP.Guid.newGuid()
    let term = termParent.createTerm(termName,1033,guid)
    ctx.load(term);
    await ctx.executeQueryPromise()
    return term
  }

  const deleteTerm = async (termParent,termName)=>{
    debug(`Deleting Term: ${termName}`)
    termParent.deleteObject()
    ctx.load(termParent);
    await ctx.executeQueryPromise()
  }

  const getTermGroup = async (name)=>{
    let termGroup = termStore.get_groups().getByName(name)
    ctx.load(termGroup);
    try {
      await ctx.executeQueryPromise()
      return termGroup
    }catch(ex){
      throw new Error(`Term Group does not exist [${name}]`)
    }
  }

  const getTermSets = async (termGroupName)=>{
    let termGroup = await getTermGroup(termGroupName)
    let termsets = termGroup.get_termSets()
    ctx.load(termsets);
    await ctx.executeQueryPromise()

    let termsetData = termsets.get_data()

    let results = []
    for(var i=0;i<termsetData.length;i++){
      let termset = termsetData[i]

      let terms = await getTerms(termset)
      
      results.push({
        type: 'TermSet',
        name: termset.get_name(),
        terms
      })
    }
    return results
  }

  const getTermSet = async (termGroupName,termSetName)=>{
    let termGroup = await getTermGroup(termGroupName)
    let termsets = termGroup.get_termSets()
    ctx.load(termsets);
    await ctx.executeQueryPromise()
    let termsetData = termsets.get_data()
    return termsetData.find(ts=>ts.get_name()===termSetName)
  }

  const getTerms=  async (parent)=>{
    let aterms = parent.get_terms()
    ctx.load(aterms)
    await ctx.executeQueryPromise()
    return aterms
  }

  const getFields = async ()=>{
    debug('Get Fields')
    let fields = ctx.get_web().get_fields()
    ctx.load(fields,'Include(Title,InternalName)');
    await ctx.executeQueryPromise()
    return fields
  }

  const createField = async (fields, fieldConfig)=>{
      let fieldName = fieldConfig.InternalName
      let fieldTitle = fieldConfig.DisplayName
      debug(`Create Field ${fieldName}`)
      let type = fieldConfig.Type
      let field = fields.get_data().find(f=>{
        let ftitle = f.get_title()
        let finternal = f.get_internalName()
        //console.log(`${fieldName} : ${ftitle}:${finternal}`)
        return ftitle===fieldTitle
      })
      if(field){
        debug(`Field exists ${fieldName}`)
      }else {
        
        let propTermSet=''
        if(type=='TaxonomyFieldType'){
              let termset = await getTermSet(fieldConfig.TermStoreGroup,fieldConfig.TermSet)
              if(termset===undefined){
                debug(`Warning: TermSet [${fieldConfig.TermSet}] is not defined`)
              }else {
                let terms = termset.get_terms()
                ctx.load(terms);
                await ctx.executeQueryPromise()
                let term = terms.get_data().find(t=>{
                  //console.log(t.get_name())
                  return t.get_name()===fieldConfig.Term
                })
                if(term){
                  //console.log(`${term.get_name()} ${termset.get_name()} ${termset.get_id()}`)
                  propTermSet=`TermSetId="${termset.get_id()}"`
                }
              }
        }

        let contained=''
        if(type==='Choice'){
          let choices = fieldConfig.SiteFieldChoices.SiteFieldChoice.map(c=>`<CHOICE>${c}</CHOICE>`)
          contained =`<CHOICES>${choices.join('')}</CHOICES>`
        }

        let listXML=''
        let showField='Term1033'
        if(type==='Lookup'){
              let list = ctx.get_web().get_lists().getByTitle(fieldConfig.LookupList)
              ctx.load(list);
              await ctx.executeQueryPromise()
              listXML = `List="${list.get_id()}" LookupField="${fieldConfig.LookupField}"`
              showField = fieldConfig.ShowField 
        }

        let xml =`<Field Type="${type}" ShowField="${showField}" DisplayName="${fieldConfig.DisplayName}" Group="${fieldConfig.Group}" Name="${fieldName}" Required="${fieldConfig.Required || 'false'}" Multi="${fieldConfig.Multi || false}" RichText="FALSE" ${propTermSet} ${listXML} >${contained}</Field>`

        debug(`Creating Field DisplayName:${fieldConfig.DisplayName} Field Name:${fieldName}`)
        field = fields.addFieldAsXml(xml, true, SP.AddFieldOptions.addToDefaultContentType);
      
        ctx.load(fields);
        await ctx.executeQueryPromise()
      }
      return field
  }

  const deleteField = async (fields,fieldConfig)=>{
    let field = fields.get_data().find(f=>f.get_title()===fieldConfig.DisplayName)
    if(field){
      try {
        debug(`Deleting Field [${field.get_title()}]`)
        field.deleteObject()
        ctx.load(field);
        await ctx.executeQueryPromise()
      }catch(ex){
        if(ex!=='Operation is not valid due to the current state of the object.') //internal fields
          console.log(ex)
      }
    }
  }

  const getSiteContentTypes = async()=>{
    let cts = ctx.get_web().get_contentTypes()
    ctx.load(cts);
    await ctx.executeQueryPromise()
    return cts.get_data()
  }

  const createSiteContentType = async(contenttype,contenttypefields)=>{
    try {
      debug(`Create contenttype: ${contenttype.Name}`)
      let content_types = ctx.get_web().get_contentTypes()
      ctx.load(content_types);
      await ctx.executeQueryPromise()
      let content_types_data = content_types.get_data()
      let ct = content_types_data.find(ct=>ct.get_name()==contenttype.Name)
      if(!ct){
        let parentCT = content_types_data.find(ct=>ct.get_name()===contenttype.ParentName)
        let c = new SP.ContentTypeCreationInformation(ctx)
        c.set_name(contenttype.Name)
        c.set_group(contenttype.Group)
        c.set_parentContentType(parentCT)
        c.set_description(contenttype.Description)
        content_types.add(c)
        ctx.load(content_types);
        //ctx.update(true)
        await ctx.executeQueryPromise()
      }
      if(contenttypefields){
          let fields = ctx.get_web().get_fields()
          ctx.load(fields);
          await ctx.executeQueryPromise()
          
          let cts = content_types.get_data()
          let ct = cts.find(ct=>ct.get_name()==contenttype.Name)
          let fieldLinks = ct.get_fieldLinks()
          ctx.load(fieldLinks);
          await ctx.executeQueryPromise()
          let fieldLinksData = fieldLinks.get_data()
          
          let fieldsAdded=false
          for(let i=0;i<contenttypefields.length;i++){
            let ctf = contenttypefields[i]
            
            let exists = fieldLinksData.find(fl=>{
              try {
                let fn =  fl.get_name()
                //debug(`Existing field: ${fn}`)
                
                return fl.get_name()===stripWhiteSpace(ctf.Name)
              }catch(ex){
                return false
              }
            })
            if(exists){
              debug(`\tLinked Field exists: ${ctf.Name}`)
              continue
            }

            let f = new SP.FieldLinkCreationInformation(ctx)
            let field = fields.get_data().find(f=>{
              let ft = f.get_title()
              let fi = f.get_internalName()
              //console.log(`${stripWhiteSpace(ctf.Name)} -checkmatch > ${ft} ${fi}`)
              return fi===stripWhiteSpace(ctf.Name)
            })
            if(field){
              debug(`Adding link to field ${ctf.Name}`)
              field.set_hidden(ctf.Hidden==="true")
              field.set_required(ctf.Required==="true")
              f.set_field(field)
              fieldLinks.add(f)
              //console.log(ctf.Name)
              fieldsAdded=true
              try {
                ctx.load(fieldLinks);
                ct.update(true)
                await ctx.executeQueryPromise()
              } catch(ex){
                debug(`Problem linking to field ${ctf.Name} ${ex}`)  
              }
            }else {
              debug(`Missing Field ${ctf.Name}`)
            }
          }
          if(fieldsAdded){
            ctx.load(fieldLinks);
            ct.update(true)
            await ctx.executeQueryPromise()
            let d = fieldLinks.get_data()
          }
      }
    }catch(ex){
      console.log(ex)
    }
  }

  const deleteSiteContentTypes = async (contenttypes,config)=>{
    let ct = contenttypes.find(f=>f.get_name()===config.Name)
    if(ct){
      try {
        debug(`Deleting ${ct.get_name()}`)
        ct.deleteObject()
        ctx.load(ct);
        await ctx.executeQueryPromise()
      }catch(ex){
        console.log(ex)
      }
    }
  }

  const queryLists = async(name)=>{
      let lists = ctx.get_web().get_lists()
      ctx.load(lists);
      await ctx.executeQueryPromise()
      let listdata = lists.get_data()
      return listdata
  }

const getList = async(name)=>{
    let lists = ctx.get_web().get_lists()
    ctx.load(lists);
    await ctx.executeQueryPromise()
    let listdata = lists.get_data()
    let list = listdata.find(l=>l.get_title()===name)
    return list
}

const createList = async(listdefinition)=>{  
  debug(`Create List ${listdefinition.Name}`)
  let existing = null
  let lists = ctx.get_web().get_lists()
  try {
    existing = lists.getByTitle(listdefinition.Name)
    ctx.load(existing);
    await ctx.executeQueryPromise()
  }catch(ex){
    existing = null
    //Doesnt exist
  }
  if(existing){
    debug(`List exists ${listdefinition.Name}`)
  } else{
    try {
      let c = new SP.ListCreationInformation(ctx)
      c.set_title(listdefinition.Name)
      let listType = listdefinition.Type ==='DocumentLibrary' ? SP.ListTemplateType.documentLibrary :  SP.ListTemplateType.genericList
      c.set_templateType(listType)
      c.set_description(listdefinition.Description)
      existing = lists.add(c)
      ctx.load(existing)
      await ctx.executeQueryPromise()
    }catch(ex){
      debug(`Problem creating list ${ex}`)
    }
  }
  
  try {
    existing = lists.getByTitle(listdefinition.Name)
    ctx.load(existing);
    await ctx.executeQueryPromise()
  }catch(ex){
    existing = null
    //Doesnt exist
  }

  if(listdefinition.EnableVersioning)
    existing.set_enableVersioning(listdefinition.EnableVersioning)
  if(listdefinition.ForceCheckout)
    existing.set_forceCheckout(listdefinition.ForceCheckout)
  // if(listdefinition.RemoveDefaultContentType)
  //   existing.set_removeDefaultContentType(listdefinition.RemoveDefaultContentType)
  if(listdefinition.OnQuickLaunch)
    existing.set_onQuickLaunch(listdefinition.OnQuickLaunch)
  // if(listdefinition.InheritPermissions)
  //   existing.set_inheritPermisions(listdefinition.InheritPermissions)
  if(listdefinition.EnableFolderCreation)
    existing.set_enableFolderCreation(listdefinition.EnableFolderCreation)
  if(listdefinition.ContentTypeBindings)
    existing.set_contentTypesEnabled(true)

  existing.update(true)
  await ctx.executeQueryPromise()

  if(listdefinition.FolderData){
    for(let i=0;i<listdefinition.FolderData.Folder.length;i++){
      let folderDef = listdefinition.FolderData.Folder[i]
      try {
        debug(`Creating folder ${folderDef.Name} on this ${listdefinition.Name}`)
        let itemCreateInfo = new SP.ListItemCreationInformation();
        itemCreateInfo.set_underlyingObjectType(SP.FileSystemObjectType.folder);
        itemCreateInfo.set_leafName(folderDef.Name);
        let oListItem = existing.addItem(itemCreateInfo);
        oListItem.set_item("Title", folderDef.Name);
        oListItem.update();
        ctx.load(oListItem);
        await ctx.executeQueryPromise()
      }catch(ex){
        if(ex.indexOf('HREF')>=0)
          debug('Ignoring HREF List Creation Error')
        else
          debug(`Problem Creating folder ${folderDef.Name} on list ${listdefinition.Name} ${ex}`)
      }
    }
  }

  if(listdefinition.ContentTypeBindings){

    let content_types = ctx.get_web().get_contentTypes()
    ctx.load(content_types);
    await ctx.executeQueryPromise()  
    let content_types_data = content_types.get_data()
      
    let contenttypes = listdefinition.ContentTypeBindings.ContentTypeBinding
    if(!Array.isArray(contenttypes)){
      contenttypes=[contenttypes]
    }
    let listContentTypes = existing.get_contentTypes()
    ctx.load(listContentTypes)
    await ctx.executeQueryPromise()

    for(let i=0;i<contenttypes.length;i++){
      let contenttype = contenttypes[i]
      
      let ct = content_types_data.find(ct=>ct.get_name()==contenttype.Name)
        if(ct){
          try {
            debug(`Adding content type ${contenttype.Name} to ${listdefinition.Name}`)
            listContentTypes.addExistingContentType(ct)
            ctx.load(existing)
            await ctx.executeQueryPromise()
          }catch(ex){
            debug(ex)
          }
        }else {
          debug(`Missing content type ${contenttype.Name} for list ${listdefinition.Name}`)
        }
      }
  }
  if(listdefinition.FieldRef){
    let fields = ctx.get_web().get_fields()
    ctx.load(fields);
    await ctx.executeQueryPromise()  
    let fields_data = fields.get_data()

    for(let i=0;i<listdefinition.FieldRef.length;i++){
      let fieldDefName = listdefinition.FieldRef[i].Name
      
      let exists = fields_data.find(f=>f.get_title()===fieldDefName)
      // if(exists){
      //   exists.update(true)
      //   ctx.load(exists)
      //   await ctx.executeQueryPromise()
      // } else {
      //   try {
      //     debug(`Adding field ${fieldDefName} to ${listdefinition.Name}`)
      //     //let c = new SP.FieldCreationInformation(ctx)    
      //     //c.set_name(fieldDefName)
          
      //     let xml = `<Field DisplayName="${fieldDefName}"/>`
      //     let field = fields.addFieldAsXml(xml,true, SP.AddFieldOptions.defaultValue)
      //     //field.update(true)
      //     ctx.load(field)
      //     await ctx.executeQueryPromise()
      //   }catch(ex){
      //     debug(ex)
      //   }
      // }
    }
  }
  return true
}

  const deleteList = async (list)=>{
    try {
      debug(`Deleting List ${list.get_title()}`)
      list.deleteObject()
      ctx.load(list);
      await ctx.executeQueryPromise()
    }catch(ex){
      console.log(ex)
    }
  }

  const queryListItems = async(name)=>{
    let list = ctx.get_web().get_lists().getByTitle(name)
    ctx.load(list);
    await ctx.executeQueryPromise()
    if(list){
      var camlQuery = new SP.CamlQuery();
      let listItems = list.getItems(camlQuery)
      ctx.load(listItems);
      await ctx.executeQueryPromise()
      return listItems.get_data()
    }else {
      debug(`No such list ${name}`)
      return []
    }
  }

  const createListItem = async(listitemdefinition)=>{  
    debug(`Create ListItem ${listitemdefinition.body.Title}`)

    var oList = ctx.get_web().get_lists().getByTitle(listitemdefinition.list);

    let contenttypes = oList.get_contentTypes()
    ctx.load(contenttypes);  
    await ctx.executeQueryPromise()

    let contenttype = contenttypes.get_data().find(ct=>ct.get_name()===listitemdefinition.contentType)
    if(!contenttype){
      debug(`Unable to create listitem, missing contenttype [${contenttype}]`)
      return
    }

  let fields = oList.get_fields()
  ctx.load(fields);  
  await ctx.executeQueryPromise()

  // fields.get_data().forEach(f=> {
  //   console.log(f.get_title())
  // })

    var itemCreateInfo = new SP.ListItemCreationInformation();
    let oListItem = oList.addItem(itemCreateInfo);
    oListItem.set_item('ContentTypeId', contenttype.get_id())
    let keys = Object.keys(listitemdefinition.body)
    for(let i=0;i<keys.length;i++){
      let key = keys[i]
      let value = listitemdefinition.body[key]
      oListItem.set_item(key, value);
    }
    oListItem.update();

    ctx.load(oListItem);  
    try {
      await ctx.executeQueryPromise()
    }catch(ex){
      throw ex
    }
  }

  const deleteListItem = async (itemname, listItem)=>{
    try {
      debug(`Deleting List Item ${itemname}`)
      listItem.deleteObject()
      ctx.load(listItem);
      await ctx.executeQueryPromise()
    }catch(ex){
      debug(ex)
    }
  }

  //https://www.codesharepoint.com/jsom/73/group
  const getSiteGroups = async ()=>{
    let existingsitegroups = ctx.get_web().get_siteGroups()
    ctx.load(existingsitegroups);
    await ctx.executeQueryPromise()
    return existingsitegroups
  }

  //https://www.codesharepoint.com/jsom/73/group
  const createSiteGroup = async (sitegroup)=>{
    let web = ctx.get_web()
    let existingsitegroups = ctx.get_web().get_siteGroups()
    ctx.load(existingsitegroups);
    await ctx.executeQueryPromise()
    let existingdata  = existingsitegroups.get_data()
    let existing = existingdata.find(g=>{
        let title = g.get_title()
        //console.log(title)
        let type = g.get_typedObject()
        let ui = g.get_isHiddenInUI()
        return title===sitegroup.Name
    })
    if(existing){
      debug(`Site Groups Exists ${sitegroup.Name}`)
    }else {
      var itemCreateInfo = new SP.GroupCreationInformation();
      itemCreateInfo.set_title(sitegroup.Name)
      itemCreateInfo.set_description(sitegroup.Description)
      existing = existingsitegroups.add(itemCreateInfo);
      existing.set_onlyAllowMembersViewMembership(false)
      ctx.load(existingsitegroups);
      await ctx.executeQueryPromise()
      debug(`Site Group created ${sitegroup.Name}`)
    }
    // existing = ctx.get_web().get_siteGroups().getByName(sitegroup.Name)
    // existing.update()
    // await ctx.executeQueryPromise()

    let assignments = web.get_roleAssignments();
    ctx.load(assignments)
    await ctx.executeQueryPromise()

    // let asgns = assignments.get_data()
    //   let bs = asgns[0].get_roleDefinitionBindings()
    //   ctx.load(bs)
    //   await ctx.executeQueryPromise()
    //   let bd = bs.get_data()
    //   console.log(bd)
    
    var role = web.get_roleDefinitions().getByName(sitegroup.PermissionLevel);
    var binding = new SP.RoleDefinitionBindingCollection(ctx);
    binding.add(role)

    var role = assignments.add(existing, binding);
    ctx.load(existing);
    await ctx.executeQueryPromise()

    return existing
  }

  const deleteSiteGroup = async (sitegroup, existingsitegroups)=>{
    try {
      debug(`Deleting Site Groups ${sitegroup.Name}`)
      
      let existing = existingsitegroups.get_data().find(g=>g.get_title()===sitegroup.Name)
      if(existing){
        existingsitegroups.remove(existing)
        ctx.load(existingsitegroups);
        await ctx.executeQueryPromise()
      }
        
    }catch(ex){
      debug(ex)
    }
  }


  const getPermissionLevels = async ()=>{
    let existingroles = ctx.get_web().get_roleDefinitions()
    ctx.load(existingroles);
    await ctx.executeQueryPromise()
    return existingroles
  }
  //https://www.codesharepoint.com/jsom/create-permission-level-in-sharepoint-using-jsom
  const createPermissionLevel = async (permissionlevel)=>{
    let web = ctx.get_web()
    let existingroles = web.get_roleDefinitions()
    ctx.load(existingroles);
    await ctx.executeQueryPromise()
    let existingdata  = existingroles.get_data()
    let existing = existingdata.find(g=>{
        let title = g.get_title()
        //console.log(title)
        let type = g.get_typedObject()
        let ui = g.get_isHiddenInUI()
        return title===permissionlevel.Name
    })
    if(existing){
      debug(`PermissionLevel Exists ${permissionlevel.Name}`)
    }else {
      
      var itemCreateInfo = new SP.GroupCreationInformation();
      itemCreateInfo.set_title(sitegroup.Name)
      itemCreateInfo.set_description(sitegroup.Description)
      existing = existingsitegroups.add(itemCreateInfo);
      existing.set_onlyAllowMembersViewMembership(false)
      ctx.load(existingsitegroups);
      await ctx.executeQueryPromise()
      debug(`Site Group created ${sitegroup.Name}`)
    }
    // existing = ctx.get_web().get_siteGroups().getByName(sitegroup.Name)
    // existing.update()
    // await ctx.executeQueryPromise()
    return existing
  }
  const deletePermissionLevel = async (permissionlevel, existingpermissionlevels)=>{
    try {
      debug(`Deleting PermissionLevel ${permissionlevel.Name}`)
      let existing = existingpermissionlevels.get_data().find(g=>g.get_title()===sitegroup.Name)
      if(existing){
        existingpermissionlevels.remove(existing)
        ctx.load(existingpermissionlevels);
        await ctx.executeQueryPromise()
      }
    }catch(ex){
      debug(ex)
    }
  }

  //https://docs.microsoft.com/en-us/sharepoint/dev/sp-add-ins/complete-basic-operations-using-javascript-library-code-in-sharepoint#create-read-update-and-delete-files
  const upsertFile = async(fileName,fileContent)=>{  
    debug(`Create File ${fileName}`)
    const web = ctx.get_web()

    const oList = web.get_lists().getByTitle("Documents");
    const fileCreateInfo = new SP.FileCreationInformation();
    fileCreateInfo.set_url(fileName);

    const content = new SP.Base64EncodedByteArray()
    const contentString = fileContent.toString('latin1')
    for (var i = 0; i < contentString.length; i++) {
        content.append(contentString.charCodeAt(i));
    }
    fileCreateInfo.set_content(content);
    fileCreateInfo.set_overwrite(true);
    const files = oList.get_rootFolder().get_files()
    const newFile = files.add(fileCreateInfo);
    try {
        //ctx.load(newFile);    
        //await ctx.executeQueryPromise()

        let wfURL = newFile.getWOPIFrameUrl(SP.Utilities.SPWOPIFrameAction.view)
        ctx.load(newFile);    
        await ctx.executeQueryPromise()
                
        return {
          editUrl: wfURL.get_value(),
          downloadUrl:  options.sharepointTenant + newFile.get_serverRelativeUrl()
        }
    }catch(ex){
        console.log(ex)        
    }
  }
  

  return {
    getFields,
    getTermSet,
    getTermSets,
    getTerms,
    createTermGroup,
    createTermSet,
    createTerm,
    deleteTerm,
    createField,
    deleteField,
    getSiteContentTypes,
    createSiteContentType,
    deleteSiteContentTypes,
    queryLists,
    getList,
    createList,
    deleteList,
    queryListItems,
    createListItem,
    deleteListItem,

    getSiteGroups,
    createSiteGroup,
    deleteSiteGroup,

    createPermissionLevel,
    deletePermissionLevel,
    getPermissionLevels,

    upsertFile,
  }
 
}